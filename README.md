# **epmel**: Extra Perl Modules for Enterprise Linux

**[Browse the yum repo.](https://harbottle.gitlab.io/epmel/7/x86_64)**

A yum repo of RPM files containing Perl [CPAN](http://www.cpan.org/) modules
not available in the standard repos. The packages are suitable for CentOS 7 (and
RHEL, Oracle Linux, etc.). Ensure you also have the
[EPEL](https://fedoraproject.org/wiki/EPEL) repo enabled.

Modules are converted automatically using
[GitLab CI](https://about.gitlab.com/gitlab-ci/) and the excellent
[fpm](https://github.com/jordansissel/fpm) RubyGem.  The yum repo is hosted
courtesy of [GitLab Pages](https://pages.gitlab.io/).

## Quick Start

```bash
# Install the EPEL repo
sudo yum -y install epel-release

# Install the epmel repo
sudo yum -y install https://harbottle.gitlab.io/epmel/7/x86_64/epmel-release.rpm
```

After adding the repo to your system, you can install
[available packages](https://harbottle.gitlab.io/epmel/7/x86_64) using `yum`.

## Why?
EPMEL is an easy way to install Perl modules on your CentOS box using the
native `yum` package manager.  Additional modules can be easily added to the
[`packages.yml`](packages.yml) file to expand the repo in line with user needs.

## Requesting additional modules

[File a new issue](https://gitlab.com/harbottle/epmel/issues/new).
